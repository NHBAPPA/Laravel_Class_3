<?php
namespace PHPLRV;

echo __LINE__."<hr>";

echo "<br>";
echo __FILE__."<hr>";

echo "<br>";
echo __DIR__."<hr>";

function doSomething(){

    echo __FUNCTION__."<hr>";

}
doSomething();

echo __NAMESPACE__." namespace<hr>";

?>

<?php
namespace XYZ;

class MyClass{
    use MyTrait;
    public function doSomethingMethod(){


        echo __CLASS__."<hr>";
        echo __METHOD__."<hr>";

    }

}
$obj=new MyClass();
$obj->doSomethingMethod();
$obj->processSth();

trait MyTrait{

    public function processSth(){

        echo __TRAIT__."<hr>";
    }

}


echo __NAMESPACE__." namespace<hr>";
